#!/usr/bin/env perl

use Boilerplate;

use Table;

my $TABLE = Table->new(1, 3, 1, 3);

dump($TABLE);

$TABLE->load(1..9);

say $TABLE->get(1, 1);
say $TABLE->get(3, 3);

say $TABLE->to_string;
