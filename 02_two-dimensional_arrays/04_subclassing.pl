#!/usr/bin/env perl

use Boilerplate;

use Table;

{
    package Matrix;
    use parent 'Table';

    sub new($class) {
        my $self = $class->SUPER::new(1, 3, 1, 3);
        bless $self, $class;
    }

    sub add($self, $matrix) {
        my $result = Matrix->new;
        for my $row ($self->{rows}->{lo_bound} .. $self->{rows}->{up_bound}) {
            $result->{rows}->put($self->{rows}->get($row)->add($matrix->{rows}->get($row)), $row);
        }
        return $result;
    }
}

my $DATA = Matrix->new;

$DATA->load(1..9);

say $DATA->to_string;

say $DATA->add($DATA)->to_string;
