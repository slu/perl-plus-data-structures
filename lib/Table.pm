package Table;

use 5.028;
use strict;
use warnings;
use open qw(:std :utf8);
use feature qw(signatures);
no warnings qw(experimental::signatures);
no bareword::filehandles;
no multidimensional;
use utf8;

use Array;

# code:
sub new($class, $row_lo_bound, $row_up_bound, $col_lo_bound, $col_up_bound) {
    my $rows = Array->new($row_lo_bound, $row_up_bound);
    for my $row_index ($row_lo_bound .. $row_up_bound) {
        $rows->put(Array->new($col_lo_bound, $col_up_bound), $row_index);
    }
    my $self = { rows => $rows };
    bless $self, $class;
}

sub put($self, $value, $row, $col) {
    $self->{rows}->get($row)->put($value, $col);
}

sub get($self, $row, $col) {
    return $self->{rows}->get($row)->get($col);
}

sub max($self) {
    my $max;
    for my $row ($self->{rows}->{lo_bound} .. $self->{rows}->{up_bound}) {
        my $row_max = $self->{rows}->get($row)->max();
        $max = $row_max if !defined $max || $max < $row_max;
    }
    return $max;
}

sub load($self, @values) {
    my $index = 0;
    for my $row ($self->{rows}->{lo_bound} .. $self->{rows}->{up_bound}) {
        $index += $self->{rows}->get($row)->load(@values[$index..$#values]);
    }
}

sub to_string($self, $field_width=undef) {
    $field_width = length($self->max()) if !defined $field_width;
    my $str = "[";
    for my $row ($self->{rows}->{lo_bound} .. $self->{rows}->{up_bound}) {
        $str .= $self->{rows}->get($row)->to_string($field_width);
        $str .= ",\n " if $row < $self->{rows}->{up_bound};
    }
    return $str . "]";
}

1;
