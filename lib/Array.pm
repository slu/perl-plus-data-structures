package Array;

use Boilerplate;

=head1 NAME

Array - fixed length array that supports arbitrary index bounds

=head1 SYNOPSIS

    use Array;
    my $array = Array->new(1,10);
    print $array->to-string,"\n";

=head1 DESCRIPTION

Simple implementation of an Array.

=head2 Methods

=over 12

=item C<new>

Returns a new Array

=cut
sub new($class, $lo_bound, $up_bound) {
    croak "Lower bound must be lower or equal to upper bound" if $lo_bound > $up_bound;

    my @array;
    $#array = $up_bound - $lo_bound;
    my $self = { elements => \@array,
                 lo_bound => $lo_bound,
                 up_bound => $up_bound };
    bless $self, $class;
}


sub _assert_bounds($self, $index) {
    my $is_within_bounds = $index >= $self->{lo_bound} && $index <= $self->{up_bound};
    croak "Index ($index) out of bounds" unless $is_within_bounds;
}


=item C<put>

Returns a new Array

=cut
sub put($self, $value, $index) {
    $self->_assert_bounds($index);
    $self->{elements}->[$index - $self->{lo_bound}] = $value;
}


=item C<get>

Returns a new Array

=cut
sub get($self, $index) {
    $self->_assert_bounds($index);
    return $self->{elements}->[$index - $self->{lo_bound}];
}


=item C<get>

Returns a new Array

=cut
sub swap($self, $index1, $index2) {
    $self->_assert_bounds($index1);
    $self->_assert_bounds($index2);
    my $temp = $self->get($index1);
    $self->put($self->get($index2), $index1);
    $self->put($temp, $index2);
}


=item C<slice>

Returns a new Array

=cut
sub slice($self, $lo_bound, $up_bound) {
    $self->_assert_bounds($lo_bound);
    $self->_assert_bounds($up_bound);
    my $slice = Array->new($lo_bound, $up_bound);
    for my $index ($lo_bound .. $up_bound) {
        $slice->put($self->get($index), $index);
    }
    return $slice
}


=item C<len>

Returns a new Array

=cut
sub len($self) {
    return $self->{up_bound} - $self->{lo_bound} + 1;
}


=item C<max>

Returns the maximum value from the array.

=cut
sub max($self) {
    my $max;
    for my $index ($self->{lo_bound} .. $self->{up_bound}) {
        next if !defined $self->get($index);
        $max = $self->get($index) if !defined $max || $max < $self->get($index);
    }
    return $max;
}


=item C<load>

Returns a new Array

=cut
sub load($self, @values) {
    my $v_index = 0;
    for my $index ($self->{lo_bound} .. $self->{up_bound}) {
        $self->put($values[$v_index++], $index);
    }
    return $v_index;
}


=item C<add>

Returns a new Array

=cut
sub add($self, $array) {
    croak "Only arrays of equal length can be added" if $self->len != $array->len;
    my $result = Array->new($self->{lo_bound}, $self->{up_bound});
    my $other_i = $array->{lo_bound};
    for my $my_i ($self->{lo_bound} .. $self->{up_bound}) {
        croak "Cannot add to an undefined value" if !defined $self->get($my_i);
        croak "Cannot add an undefined value" if !defined $array->get($other_i);
        $result->put($self->get($my_i) + $array->get($other_i++), $my_i);
    }
    return $result;
}


=item C<to_string>

Returns a new Array

=cut
sub to_string($self, $field_width=1) {
    my $str = "[";
    for my $index ($self->{lo_bound} .. $self->{up_bound}) {
        my $val = defined $self->get($index) ? $self->get($index) : '□';
        $str .= sprintf '%*2$s', $val, $field_width;
        $str .= ", " if $index < $self->{up_bound};
    }
    return $str . "]";
}

=back

=cut

1;
