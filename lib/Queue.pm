package Queue;

use 5.028;
use strict;
use warnings;
use open qw(:std :utf8);
use feature qw(signatures);
no warnings qw(experimental::signatures);
no bareword::filehandles;
no multidimensional;
use utf8;

use Array;

# code:
sub new($class, $size) {
    my $self = { elements => Array->new(1, $size),
                 front => 1,
                 rear => 1 };
    bless $self, $class;
}

#sub clear($self) {
#    $self->{index} = 0;
#}

sub enq($self, $value) {
    die "Queue full" if $self->is_full;
    $self->{elements}->put($value, $self->{rear}++);
}

sub deq($self) {
    die "Queue empty" if $self->is_empty;
    return $self->{elements}->get($self->{front}++);
}

sub is_empty($self) {
    return $self->{rear} - $self->{front} == 0;
}

sub is_full($self) {
    return $self->{rear} - $self->{front} == $self->{elements}->{up_bound};
}

#sub load($self, @values) {
#    my $index = 0;
#    for my $row ($self->{rows}->{lo_bound} .. $self->{rows}->{up_bound}) {
#        $index += $self->{rows}->get($row)->load(@values[$index..$#values]);
#    }
#}

sub to_string($self) {
    return "<empty>" if $self->is_empty;
    return $self->{elements}->slice($self->{front}, $self->{rear}-1)->to_string;
}

1;
