package Stack;

use 5.028;
use strict;
use warnings;
use open qw(:std :utf8);
use feature qw(signatures);
no warnings qw(experimental::signatures);
no bareword::filehandles;
no multidimensional;
use utf8;

use Carp;

use Array;

# code:
sub new($class, $size) {
    my $self = { elements => Array->new(1, $size),
                 index => 0 };
    bless $self, $class;
}

sub clear($self) {
    $self->{index} = 0;
}

sub push($self, $value) {
    croak "Stack full" if $self->is_full;
    $self->{elements}->put($value, ++$self->{index});
}

sub pop($self) {
    croak "Stack empty" if $self->is_empty;
    return $self->{elements}->get($self->{index}--);
}

sub top($self) {
    croak "Stack empty" if $self->is_empty;
    return $self->{elements}->get($self->{index});
}

sub is_empty($self) {
    return $self->{index} == 0;
}

sub is_full($self) {
    return $self->{index} == $self->{elements}->{up_bound};
}

sub load($self, @values) {
    my $index = 0;
    for my $row ($self->{rows}->{lo_bound} .. $self->{rows}->{up_bound}) {
        $index += $self->{rows}->get($row)->load(@values[$index..$#values]);
    }
}

sub to_string($self) {
    return "<empty>" if $self->is_empty;
    return $self->{elements}->slice(1, $self->{index})->to_string;
}

1;
