package Car;

use 5.028;
use strict;
use warnings;
use open qw(:std :utf8);
use feature qw(signatures);
no warnings qw(experimental::signatures);
no bareword::filehandles;
no multidimensional;
use utf8;

# code:
sub new($class, $year, $color, $price) {
    my $self = { year => $year,
                 color => $color,
                 price => $price };
    bless $self, $class;
}

sub get_year($self) {
    return $self->{year};
}

sub to_string($self) {
    return "year:  $self->{year}\n"
      . "color: $self->{color}\n"
      . "price: $self->{price}";
}
1;
