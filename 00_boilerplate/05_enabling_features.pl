use Boilerplate;

my $variable = "foo";
say $variable;

sub foo($value) {
    my $temp = "'$variable'";
    say "temp=$temp";
}

foo($variable);

my $island = "ø"; # In danish the word for island is ø
say $island;

my $ø = "island";
say $ø;

open(my $file, '<', $0) or die $!;
while(<$file>) {
    print if /\$(ø|island)/;
};
close $file;

my $dict = { da => $island, en => $ø };
dump($dict);
say $dict->{da};
