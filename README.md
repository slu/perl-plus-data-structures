[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) [![Written in Perl](https://img.shields.io/badge/Language-Perl-blue)](https://www.perl.org/)
----
# Perl Plus Data Structures

One of my favorite text books at university was [Pascal plus Data
Structures, Algorithms, and Advanced
Programming](https://archive.org/details/pascalplusdatast0000dale_c8h9)
by Nell Dale and Susan C. Lilly. I had an idea of re-reading it and
playing with the concepts in Perl. The (not yet complete) result is in
this repository.

I've added a script, `run.sh` to run/show all the examples. The script
needs the `pygmentize` command to be installed, and it's part of
[Pygments](https://pygments.org) and can be installed using the
following:

```
pip install Pygments
```

## References

* Badges hosted by https://shields.io/
* GPLv3 badge found at https://gist.github.com/lukas-h/2a5d00690736b4c3a7ba