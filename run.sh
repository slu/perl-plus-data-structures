#!/usr/bin/env bash

set -u

if [[ ! -v NO_COLOR && ! -v NO_ANSI ]]; then
    export ANSI_BOLD="\\033[1m"
    export ANSI_BLACK="\\033[30m"
    export ANSI_RED="\\033[31m"
    export ANSI_GREEN="\\033[32m"
    export ANSI_YELLOW="\\033[33m"
    export ANSI_BLUE="\\033[34m"
    export ANSI_MAGENTA="\\033[35m"
    export ANSI_CYAN="\\033[36m"
    export ANSI_WHITE="\\033[37m"
    export ANSI_REVERSE="\\033[7m"
    export ANSI_RESET="\\033[0m"
fi

function goto() {
    local line=$1
    local row=${2:-0}
    
    echo -en "\\033[${line};${row}H"
}

function print_example_title() {
    clear

    local title=$1
    local section_title=$2
    local title_len=${#title}
    local section_title_len=${#section_title}
    local fill_len=$(( COLUMNS - title_len - section_title_len ))
    local fill
    fill=$(printf -- " %.0s" $(seq 1 "$fill_len"))

    echo -e "${ANSI_REVERSE}${title//_/ }${fill}${section_title//_/ }${ANSI_RESET}"
}

function print_section_title() {
    clear

    local title=$1
    local length=${#title}
    local start_row=$(( (COLUMNS - length) / 2))
    local start_line=$(( (LINES - 2) / 2))

    goto $start_line $start_row
    echo -en "${ANSI_BOLD}${title//_/ }${ANSI_RESET}"
    goto $((start_line + 1)) $start_row
    printf -- "=%.0s" $(seq 1 "$length")
}

function print_code() {
    local filename=$1
    local in_code_block
    [[ ${filename#*.} == 'pm' ]] && in_code_block=1
    {
        local IFS=""
        while read -r line; do
            [[ -v in_code_block ]] && echo "$line"
            [[ $line == 'use Boilerplate;' || $line == '# code:' ]] && in_code_block=1
        done < "$filename" | pygmentize -f terminal -l perl -O linenos=1 | head -n-1
    }
}

function prompt() {
    local msg=${1:-Press <enter> to continue}
    echo -ne "\\n${msg} "; read -r
}

for section in *; do
    [[ ! -d $section || ! $section =~ ^[0-9]{2}_.* ]] && continue

    print_section_title "${section^^}"
    echo -e "\\033[$LINES;0H"
    prompt

    (
        cd "$section" || exit
        for example in *.p[lm]; do
            print_example_title "${example%.p[lm]}" "$section"
            print_code "$example"
            if [[ ${example#*.} == 'pl' ]]; then
                prompt "Press <enter> to run example "
                output="$(perl -I ../lib "$example" 2>&1)"
                printf -- "_%.0s" $(seq 1 $COLUMNS)
                echo -e "\\n${ANSI_GREEN}${output}${ANSI_RESET}"
                printf -- "‾%.0s" $(seq 1 $COLUMNS)
            fi
            prompt
        done
   )
done
