#!/usr/bin/env perl

use Boilerplate;

use Array;

my $DATA = Array->new(-4, 5);

dump($DATA);
say $DATA->to_string;

$DATA->put(1, -4);
$DATA->put(2, -3);

say $DATA->to_string;

say $DATA->get(-4);

try {
    $DATA->put(20, 19);
} catch ($e) {
    say $e;
}

$DATA->load(1..10);
say $DATA->to_string;

say $DATA->add($DATA)->to_string;
