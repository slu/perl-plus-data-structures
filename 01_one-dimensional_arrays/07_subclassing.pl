#!/usr/bin/env perl

use Boilerplate;

use Array;

{
    package Vector;
    use parent 'Array';

    sub new($class) {
        my $self = $class->SUPER::new(1, 10);
        bless $self, $class;
    }

    sub dot($self, $vector) {
        my $result;
        for my $index ($self->{lo_bound} .. $self->{up_bound}) {
            $result += $self->get($index) * $vector->get($index);
        }
        return $result;
    }
}

my $DATA = Vector->new;
dump($DATA);

$DATA->put($_, $_) for 1..10;

say $DATA->to_string;

say $DATA->dot($DATA);
