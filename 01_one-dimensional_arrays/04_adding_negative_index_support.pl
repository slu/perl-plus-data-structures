#!/usr/bin/env perl

use Boilerplate;

sub array($lo_bound, $up_bound) {
    if ($lo_bound > $up_bound) {
        warn "Lower bound must be lower or equal to upper bound"; return;
    }
    my @array;
    $#array = $up_bound - $lo_bound;
    return { elements => \@array,
             lo_bound => $lo_bound,
             up_bound => $up_bound };
}

sub put($array, $value, $index) {
    if ($index < $array->{lo_bound} || $index > $array->{up_bound}) {
        warn "Index ($index) out of bounds"; return;
    }
    $array->{elements}->[$index - $array->{lo_bound}] = $value;
}

my $DATA = array(-4, 5);

put($DATA, 1, -4);
put($DATA, 2, -3);

dump($DATA);

put($DATA, 20, 19);
