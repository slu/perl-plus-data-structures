#!/usr/bin/env perl

use Boilerplate;

sub put($array, $value, $index) {
    if ($index < 0 || $index > $#{$array}) {
        warn "Index ($index) out of bounds"; return;
    }
    $array->[$index] = $value;
}

my $DATA; $#{$DATA} = 4;

put($DATA, 1, 0);
put($DATA, 2, 1);

dump($DATA);

put($DATA, 6, 5);
