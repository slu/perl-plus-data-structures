#!/usr/bin/env perl

use 5.028;
use strict;
use warnings;
use open qw(:std :utf8);
use feature qw(signatures);
no warnings qw(experimental::signatures);
no bareword::filehandles;
no multidimensional;
use utf8;

use FindBin;
use lib "$FindBin::RealBin/lib";

# code:
use Stack;

sub check_reverse($str) {
    my $chars = Stack->new(length($str));
    my $index;
    for ($index = 0; substr($str, $index, 1) ne '.'; $index++) {
        $chars->push(substr($str, $index, 1));
    }
    my $reverse = 1;
    for ($index++; !$chars->is_empty(); $index++) {
        if (substr($str, $index, 1) ne $chars->pop()) {
            $reverse = 0;
        }
    }
    return $reverse;
}

for my $str (qw(123.321 abc.abc . kødpålæg.gælåpdøk)) {
    say check_reverse($str), " $str";
}
