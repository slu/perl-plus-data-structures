#!/usr/bin/env perl

use 5.028;
use strict;
use warnings;
use open qw(:std :utf8);
use feature qw(signatures);
no warnings qw(experimental::signatures);
no bareword::filehandles;
no multidimensional;
use utf8;

use Data::Dump qw(dump);

# code:
my $STACK = [];

dump($STACK);

push @$STACK, 2; dump($STACK);
push @$STACK, 3; dump($STACK);
push @$STACK, 5; dump($STACK);

say pop @$STACK; dump($STACK);
say pop @$STACK; dump($STACK);
say pop @$STACK; dump($STACK);


unshift @$STACK, 2; dump($STACK);
unshift @$STACK, 3; dump($STACK);
unshift @$STACK, 5; dump($STACK);

say shift @$STACK; dump($STACK);
say shift @$STACK; dump($STACK);
say shift @$STACK; dump($STACK);
