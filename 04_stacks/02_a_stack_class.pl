#!/usr/bin/env perl

use 5.028;
use strict;
use warnings;
use open qw(:std :utf8);
use feature qw(signatures);
no warnings qw(experimental::signatures);
no bareword::filehandles;
no multidimensional;
use utf8;

use Data::Dump qw(dump);
use Nice::Try;

use FindBin;
use lib "$FindBin::RealBin/lib";

# code:
use Stack;

my $STACK = Stack->new(10);

say 'Stack: ', $STACK->to_string;

for my $number (qw(2 3 5)) {
    say "Push $number => stack";
    $STACK->push($number);
    say 'Stack: ', $STACK->to_string;
}

for (1..3) {
    say 'Pop from stack => ', $STACK->pop();
    say 'Stack: ', $STACK->to_string;
}

try { $STACK->push(0) for (0..10) } catch ($e) { say $e }

$STACK->clear();

try { $STACK->pop() for (0..10) } catch ($e) { say $e }
