#!/usr/bin/env perl

use 5.028;
use strict;
use warnings;
use open qw(:std :utf8);
use feature qw(signatures);
no warnings qw(experimental::signatures);
no bareword::filehandles;
no multidimensional;
use utf8;

use Data::Dump qw(dump);
use Nice::Try;

use FindBin;
use lib "$FindBin::RealBin/lib";

# code:
use Car;

my $CAR = Car->new(2020, 'red', 1_200_000);

dump($CAR);

say $CAR->to_string;

say $CAR->get_year;
