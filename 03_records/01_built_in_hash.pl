#!/usr/bin/env perl

use 5.028;
use strict;
use warnings;
use open qw(:std :utf8);
use feature qw(signatures);
no warnings qw(experimental::signatures);
no bareword::filehandles;
no multidimensional;
use utf8;

use Data::Dump qw(dump);

# code:
my $CAR = {
    year => 2020,
    color => 'red',
    price => 1_200_000
   };

dump($CAR);

$CAR->{model} = 'T1000';

dump($CAR);
