use Test::More tests => 9;

require_ok('Boilerplate');

my $use_boilerplate = "use Boilerplate;";
my $no_boilerplate = "no Boilerplate;";

sub check {
    my ($bad_statement, $fixed_statement, $description) = (@_);
    {
        eval "$bad_statement";
        ok(!length $@, "$description");
    }
    {
        local $SIG{__WARN__} = sub { die $_[0] };
        eval "${use_boilerplate}${bad_statement}";
        ok(length $@, "handling $description");
    }
    {
        eval "${use_boilerplate}${fixed_statement}";
        ok(!length $@, "fixed $description");
    }
    {
        eval "${use_boilerplate}${no_boilerplate}${bad_statement}";
        ok($variable == "foo", "not handling $description");
    }
}

check('my $var; print $var', 'my $var=""; print $var', 'using uninitialized value (warnings)');
check('$variable = "foo"', 'my $variable = "foo"', 'missing declaration (strict)');
