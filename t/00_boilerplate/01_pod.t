use 5.028;
use strict;
use warnings;
use open qw(:std :utf8);
use feature qw(signatures);
no warnings qw(experimental::signatures);
no bareword::filehandles;
no multidimensional;
use utf8;

use Test::Most;
use Test::Pod tests => 2;
use Pod::Coverage;

use FindBin;

pod_file_ok("$FindBin::RealBin/../../lib/Boilerplate.pm", 'Boilerplate POD is valid');

#print Pod::Coverage->new(package => 'Boilerplate')->why_unrated,"\n";

#use Data::Dump qw(dump);
#my $pd = Pod::Coverage->new(package => 'Boilerplate');
#dump($pd->why_unrated);

ok(Pod::Coverage->new(package => 'Boilerplate')->coverage == 1, 'Boilerplate POD is comprehensive');
