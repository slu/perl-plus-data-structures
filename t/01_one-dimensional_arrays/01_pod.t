use 5.028;
use strict;
use warnings;
use open qw(:std :utf8);
use feature qw(signatures);
no warnings qw(experimental::signatures);
no bareword::filehandles;
no multidimensional;
use utf8;

use Test::Most;
use Test::Pod tests => 2;
use Pod::Coverage;

use FindBin;

pod_file_ok("$FindBin::RealBin/../../lib/Array.pm", 'Array POD is valid');

ok(Pod::Coverage->new(package => 'Array')->coverage == 1, 'Arry POD is comprehensive');
