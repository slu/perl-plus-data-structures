use Test::Most tests => 21;
use utf8;

my $LO_BOUND = 0;
my $UP_BOUND = 9;
my $LO_SLICE = 4;
my $UP_SLICE = 6;

my $ARRAY;
my $ADDME;
my $SLICE;


# ----------------------------------------------------------------------
# Object creation
# ----------------------------------------------------------------------

require_ok('Array');

$ARRAY = Array->new($LO_BOUND, $UP_BOUND);
$ADDME = Array->new($LO_BOUND, $UP_BOUND);
$SLICE = $ARRAY->slice($LO_SLICE, $UP_SLICE);

dies_ok { Array->new($UP_BOUND, $LO_BOUND) } 'dies when lo_bound > up_bound';


# ----------------------------------------------------------------------
# Length
# ----------------------------------------------------------------------

ok($ARRAY->len == $UP_BOUND - $LO_BOUND + 1, 'array length');
ok($SLICE->len == $UP_SLICE - $LO_SLICE + 1, 'slice length');


# ----------------------------------------------------------------------
# Get and set values
# ----------------------------------------------------------------------

ok(!defined $ARRAY->get(5), 'default values are undefined');
ok($ARRAY->put(5, 5), 'put element');
ok($ARRAY->get(5) == 5, 'get element');

dies_ok { $ARRAY->put(0, $UP_BOUND+1) } 'put dies when index > up_bound';
dies_ok { $ARRAY->put(0, $LO_BOUND-1) } 'put dies when index < lo_bound';
dies_ok { $ARRAY->get(0, $UP_BOUND+1) } 'get dies when index > up_bound';
dies_ok { $ARRAY->get(0, $LO_BOUND-1) } 'get dies when index < lo_bound';


# ----------------------------------------------------------------------
# Max
# ----------------------------------------------------------------------

ok($ARRAY->max() == 5, 'max element');
$ARRAY->put(25, 9);
ok($ARRAY->max() == 25, 'max element after put');
$ARRAY->put(50, 0);
ok($ARRAY->max() == 50, 'max element after 2nd put');

# ----------------------------------------------------------------------
# Load and stringify
# ----------------------------------------------------------------------

ok($ARRAY->load(1..9), 'load values');
ok($ARRAY->get(5) == 6, 'get loaded element');
ok($ARRAY->to_string eq '[1, 2, 3, 4, 5, 6, 7, 8, 9, □]', 'stringify array');


# ----------------------------------------------------------------------
# Add arrays
# ----------------------------------------------------------------------

dies_ok { $ARRAY->add($ARRAY)->get(6) == 14 } 'add arrays fails when a value is undef';

$ARRAY->put(10, 9);
$ADDME->load(11..19);
dies_ok { $ARRAY->add($ADDME)->get(6) == 14 } 'add arrays fails when a value is undef';

$ADDME->put(20, 9);
ok($ARRAY->add($ADDME)->get(6) == 7+17, 'add arrays');

dies_ok { $ARRAY->add($SLICE) } 'dies when adding arrays of unequal length';
