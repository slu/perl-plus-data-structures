#!/usr/bin/env perl

use 5.028;
use strict;
use warnings;
use open qw(:std :utf8);
use feature qw(signatures);
no warnings qw(experimental::signatures);
no bareword::filehandles;
no multidimensional;
use utf8;

use Data::Dump qw(dump);

# code:
my $QUEUE = [];

dump($QUEUE);

unshift @$QUEUE, 2; dump($QUEUE);
unshift @$QUEUE, 5; dump($QUEUE);
unshift @$QUEUE, 7; dump($QUEUE);

say pop @$QUEUE; dump($QUEUE);
say pop @$QUEUE; dump($QUEUE);
say pop @$QUEUE; dump($QUEUE);


push @$QUEUE, 2; dump($QUEUE);
push @$QUEUE, 5; dump($QUEUE);
push @$QUEUE, 7; dump($QUEUE);

say shift @$QUEUE; dump($QUEUE);
say shift @$QUEUE; dump($QUEUE);
say shift @$QUEUE; dump($QUEUE);
