#!/usr/bin/env perl

use 5.028;
use strict;
use warnings;
use open qw(:std :utf8);
use feature qw(signatures);
no warnings qw(experimental::signatures);
no bareword::filehandles;
no multidimensional;
use utf8;

use Data::Dump qw(dump);
use Nice::Try;

use FindBin;
use lib "$FindBin::RealBin/lib";

# code:
use Queue;

my $QUEUE = Queue->new(10);

say $QUEUE->to_string;

$QUEUE->enq(2); say $QUEUE->to_string;
$QUEUE->enq(5); say $QUEUE->to_string;
$QUEUE->enq(7); say $QUEUE->to_string;

say $QUEUE->deq(); say $QUEUE->to_string;
say $QUEUE->deq(); say $QUEUE->to_string;
say $QUEUE->deq(); say $QUEUE->to_string;
