#!/usr/bin/env perl

use Boilerplate;

use Array;

{
    package InstrumentedArray;
    use parent 'Array';

    sub new($class, $lo_bound, $up_bound) {
        my $self = $class->SUPER::new($lo_bound, $up_bound);
        $self->{swap_count} = 0;
        $self->{get_count} = 0;
        bless $self, $class;
    }

    sub get($self, $index) {
        $self->{get_count}++;
        return $self->SUPER::get($index);
    }

    sub get_count($self) {
        return $self->{get_count};
    }

    sub swap($self, $index1, $index2) {
        $self->{swap_count}++;
        $self->SUPER::swap($index1, $index2);
    }

    sub swap_count($self) {
        return $self->{swap_count};
    }
}


sub bubble_sort1($data, $n) {
    for (my $i = 1; $i < $n; $i++) {
        for (my $j = $n; $j > $i; $j--) {
            $data->swap($j, $j-1) if ($data->get($j) < $data->get($j-1));
        }
    }
}

sub bubble_sort2($data, $n) {
    for (my $i = 1; $i < $n; $i++) {
        my $swapped = 0;
        for (my $j = $n; $j > $i; $j--) {
            if ($data->get($j) < $data->get($j-1)) {
                $data->swap($j, $j-1);
                $swapped = 1;
            }
        }
        last unless $swapped;
    }
}

my @DATA = (6,5,8,4,1,9,2,3,7);

my $DATA1 = InstrumentedArray->new(1, scalar @DATA);
$DATA1->load(@DATA);

say $DATA1->to_string;
bubble_sort1($DATA1, $DATA1->len);
say $DATA1->to_string;
printf "swaps: %d\ngets : %d\n\n", $DATA1->swap_count, $DATA1->get_count;

my $DATA2 = InstrumentedArray->new(1, scalar @DATA);
$DATA2->load(@DATA);

say $DATA2->to_string;
bubble_sort2($DATA2, $DATA1->len);
say $DATA2->to_string;
printf "swaps: %d\ngets : %d\n\n", $DATA2->swap_count, $DATA2->get_count;
