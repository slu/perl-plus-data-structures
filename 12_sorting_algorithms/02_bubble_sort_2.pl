#!/usr/bin/env perl

use Boilerplate;

use Array;

sub bubble_sort($data, $n) {
    for (my $i = 1; $i < $n; $i++) {
        my $swapped = 0;
        for (my $j = $n; $j > $i; $j--) {
            if ($data->get($j) < $data->get($j-1)) {
                $data->swap($j, $j-1);
                $swapped = 1;
            }
        }
        last unless $swapped;
    }
}

my $DATA = Array->new(1, 9);
$DATA->load(6,5,8,4,1,9,2,3,7);

say $DATA->to_string;
bubble_sort($DATA, $DATA->len);
say $DATA->to_string;
